/*Activity:
1. In the S16 folder, create an activity folder, an index.html file inside of it and link the index.js file.

2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.*/

console.log("Hello World");

/*3. Create two variables that will store two different number values
and create several variables that will store the sum, difference, product and quotient of the two numbers. Print the results in the console*/

let x =14
let y = 16
let message = "this is the message"

let sum = x + y;
console.log("The sum of the two numbers is: " + sum);

let difference = x - y;
console.log("The difference of the two numbers is: " + difference);

let product = x * y;
console.log("The product of the two numbers is: " + product);

let quotient = x / y;
console.log("The quotient of the two numbers is: " + quotient);

// 4. Using comparison operators print out a string in the console that will return a message if the sum is greater than the difference.

let isGreater = sum > difference;
console.log('The sum is greater than the difference: ' + isGreater);

/*5. Using comparison and logical operators print out a string in the console 
that will return a message if the product and quotient are both positive numbers.*/

let positive = product && quotient >= 0;
console.log("The product and the quotient are positive numbers: " + positive);

/*6. Using comparison and logical operators print out a string in the console 
that will return a message if one of the results is a negative number.*/

let negative = (sum || difference || product || quotient) >= 0;
console.log("One of the results is negative: " + negative);

/*7. Using comparison and logical operators print out a string in the console 
that will return a message if one of the results is zero.*/

let notzero = (sum || difference || product || quotient) >= 0;
console.log("All the results are not equal to zero: " + notzero);